module.exports = {
  bundler: "@vuepress/vite",

  base: '/docs/',
  dest: "./web",
  public: "./docs/public",

  plugins: [
    "@vuepress/search",
    {
      searchMaxSuggestions: 10,
    },
  ],

  title: "Docs",
  lang: "ru-RU",

  themeConfig: {
    navbar: [
      {
        text: "DVLP Карта",
        link: "https://docs.google.com/spreadsheets/d/15qUytCDIPh_OXPEkCcStb3YDq9Y-JuW0RX8Lx23dBOw/edit#gid=1054295822",
      },
      {
        text: "Develup 2022. Календарь",
        link: "https://develup.pro/calendar/2022/",
      },
      {
        text: "DVLP Vault",
        link: "https://vault.dvlp.cc",
      },
    ],

    sidebar: [
      {
        text: "Верстка",
        link: "/markup",
        children: [
          {
            text: "Масштабируемая вёрстка",
            link: "/markup/scalable.html",
          },
        ],
      },
      {
        text: "Стартовый шаблон",
        link: "/template",
        children: [
          {
            text: "Структура стилей в шаблоне",
            link: "/template/styleguide.html",
          },
          {
            text: "Базовые компоненты",
            link: "/template/components.html",
          },
        ],
      },
      {
        text: "Линтеры и Prettier",
        link: "/linters",
        children: [
          {
            text: "ESLint",
            link: "/linters/eslint.html",
          },
          {
            text: "Stylelint",
            link: "/linters/stylelint.html",
          },
          {
            text: "Prettier",
            link: "/linters/prettier.html",
          },
        ],
      },
      {
        text: "Шеринг (Open Graph)",
        link: "/share",
      },
    ],
  },
};
