---
title: Линтеры и форматирование кода

prev:
  text: Главная
  link: "/"

next:
  text: ESLint
  link: "/linters/eslint.html"
---

# Линтеры и форматирование кода

## Линтеры

- **Eslint** - [инструкция](./eslint.md)

- **Stylelint** - [инструкция](./stylelint.md)

## Форматирование кода

- **Prettier** - [инструкция](./prettier.md)

## Оф. документации

- **ESLint** - [https://eslint.org/](https://eslint.org/)
- **Stylelint** - [https://stylelint.io/](https://stylelint.io/)
- **Prettier** - [https://prettier.io/](https://prettier.io/)
