---
title: Prettier

prev:
  text: Stylelint
  link: "/template/linters/stylelint.html"
---

## Что такое Prettier

Prettier - это средство для форматирования кода, которое нацелено на использование жёстко заданных правил по оформлению программ. Важно понимать что prettier не регулирует синтаксис и правила связанные с программированием, лишь визуальное оформление. Синтаксис и правила написания кода по прежнему регулирует ESLint

## Настройка плагина для PHPStorm

В первую очередь необходимо установить плагин Prettier из Marketplace

![phpstorm-external-tools](/img/phpstorm-prettier-adding.jpg)

А затем активировать его, зайдя в настройки плагина:

1. Найти плагин в поисковой строке
2. Открыть окно настроек
3. Выбрать из выпадающего списка путь к плагину (/node_modules/....)
4. Добавить в список файлов подпадающих под форматирование (\*.vue)
5. Включить запуск Prettier при сохранении и форматировании файлов

![add-keymap](/img/phpstorm-prettier-settings.jpg)

## Настройка плагина для VS Code

1. [Поставить расширение.](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
2. В файле настроек добавить следующий код

```json
{
  // Глобально для всех типов файлов (которые поддерживает Prettier)
  "editor.defaultFormatter": "esbenp.prettier-vscode",

  // Или отдельно для требуемых
  "[javascript]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode",
  }

  // Если нужно, чтобы файлы форматировались по сохранению
  "editor.formatOnSave": true,

  // Также можно настроить отдельно для требуемых файлов
  "[javascript]": {
    "editor.formatOnSave": true,
  }
}
```

По-умолчанию расширение будет искать в папке проекта файл конфигурации, и если найдет его, настройки в нем будут применены поверх настроек в редакторе.

## Правила (плагины)

| Keywords                        | Description                                                                                                                                                                                 |
| ------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| plugin:prettier/recommended     | [Отключение базовых правил конфликтующих с prettier](https://github.com/prettier/eslint-config-prettier) (конфликтующие правила - это правила отвечающие за визуальное форматирование кода) |
| plugin:prettier-vue/recommended | [Плагин необходимый для интеграции prettier c Vue-специфичным кодом](https://github.com/meteorlxy/eslint-plugin-prettier-vue)                                                               |
| prettier                        | [Плагин prettier отвечающий за валидацию и форматирование кода](https://prettier.io/docs/en/plugins.html)                                                                                   |
