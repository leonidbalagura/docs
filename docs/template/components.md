---
title: Базовые компоненты

prev:
  text: Стартовый шаблон
  link: "/template/styleguide.md"
---

# Базовые компоненты

## Структура описания компонента

| Title                | Description                                           |
| -------------------- | ----------------------------------------------------- |
| description          | описание компонента                                   |
| dependencies         | зависимости необходимые для корректной работы         |
| props                | свойства который принимает компонент                  |
| slots (+ slot props) | слоты передаваемые в компонент ( + область видимости) |
| emits                | события бросаемые компонентом вверх                   |
