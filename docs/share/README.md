---
title: Шеринг

prev:
text: Главная
link: "/"
---

# Шеринг (он же репост/публикация)

**Шеринг** - способ распространения контента в лентах социальных сетей. Посетители сайта делают репосты в соцсети, а по ним будут приходить новые посетители. За правильность (привлекательный вид) репостов страниц сайта отвечает разметка Open Graph.

**Open Graph** - это протокол, который позволяет управлять и контролировать данные, формирующиеся в превью страниц (структурирует информацию о странице) при публикации ссылок на них. В реальности - это несколько строчек мета-тегов, добавленных в раздел `<head>` веб-страницы, которые передают соцсетям нужную информацию и указывают, какой контент использовать при репосте и как его отображать.

Open Graph был разработан и изначально использовался Facebook, но на сегодняшний день, поддерживается всеми известными соцсетями и мессенджерами.

Внутри тега <meta> указываются атрибуты property и content. Атрибут property имеет обязательные и опциональные свойства:

### Обязательные свойства

| Тег | Описание |
| --- | --- |
| og:url | [Канонический URL](https://developers.google.com/search/docs/advanced/crawling/consolidate-duplicate-urls?hl=ru), по которому доступен указанный контент. |
| og:title | Заголовок контента, текст которого может не совпадать с заголовком страницы. |
| og:image | Абсолютный URL изображения, которое нужно подтянуть к публикации при репосте. |

### Опциональные свойства

| Тег | Описание |
| --- | --- |
| og:type | [Тип мультимедиа](https://ruogp.me/#types), к которому относятся контент. Если тип не указан, по умолчанию используется значение **website**. |
| og:description | Краткое описание контента. |
| og:site_name | Название сайта. |

## Основы шеринга и частые ошибки

- **Google Docs** - [ссылка](https://docs.google.com/document/d/1inMI2rTs8u1EhEDrjUIju16pc7afFaubzYoTtthGTo0/edit#)
- **Мета-теги** - [ссылка](https://docs.google.com/document/d/13ZX1TPvvCOlFp6Ud0KREpQOmeWTvfo_MKv3PAna0-F0/edit#)
- **Шеринг Facebook** - [ссылка](https://developers.facebook.com/docs/sharing)
- **Отладчик репостов Facebook** - [ссылка](https://developers.facebook.com/tools/debug/)
- **Open Graph** - [ссылка](https://ogp.me/)

## Сброс кэша шерингов

После обновление информации уже расшаренного контента, соцсети, как правило, не подтягивают новые данные, а берут информацию из своего кеша. Чтобы сообщить им об этих изменениях, используйте следующие инструменты:

- **для Facebook** - [Sharing Debugger](https://developers.facebook.com/tools/debug/)
- **для Twitter** - [Card Validator](https://cards-dev.twitter.com/validator)
- **для LinkedIn** - [Post Inspector](https://www.linkedin.com/post-inspector/)
- **для Telegram** - [@webpagebot](https://telegram.me/webpagebot)
- **для VK** - [pages.clearCache ](https://vk.com/dev/pages.clearCache)


```js
export const shareNetworks = {
  vk: 'vk',
  fb: 'fb',
  tw: 'tw',
  ok: 'ok',
  tg: 'tg',
};

const shareWindows = {};

export function share(network, link, { title, description, image }, callback) {
  if (shareWindows?.[network]) {
    shareWindows[network].focus();
    return;
  }

  switch (network) {
    case shareNetworks.vk:
      sharePopup(
        `https://vk.com/share.php?url=${link}&title=${encodeURIComponent(
          title
        )}&description=${encodeURIComponent(description)}&image=${image}`,
        550,
        300,
        callback
      );
      break;
    case shareNetworks.fb:
      sharePopup(
        `https://www.facebook.com/sharer/sharer.php?u=${link}`,
        550,
        300,
        callback
      );
      break;
    case shareNetworks.tw:
      sharePopup(
        `https://twitter.com/intent/tweet?original_referer=${link}&tw_p=tweetbutton&url=${link}`,
        550,
        300,
        callback
      );
      break;
    case shareNetworks.ok:
      sharePopup(
        `https://connect.ok.ru/dk?cmd=WidgetSharePreview&st.cmd=WidgetSharePreview&st._aid=ExternalShareWidget_SharePreview&st.shareUrl=${link}`,
        550,
        300,
        callback
      );
      break;
    case shareNetworks.tg:
      sharePopup(
        `https://telegram.me/share/url?url=${link}`,
        550,
        300,
        callback
      );
      break;
    default:
      break;
  }

  function sharePopup(url, width = 400, height = 400, _callback) {
    shareWindows[network] = window.open(
      url,
      network,
      `height=${height},width=${width},menubar=no,toolbar=no,location=no`
    );

    const watchTimer = setInterval(() => {
      if (shareWindows?.[network].closed) {
        shareWindows[network] = null;
        clearInterval(watchTimer);

        if (typeof _callback !== 'undefined') {
          _callback();
        }
      }
    }, 200);
  }
}
```